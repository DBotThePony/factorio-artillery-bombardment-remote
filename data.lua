
local item = {
	type = 'selection-tool',
	name = 'artillery-bombardment-remote',
	subgroup = 'capsule',
	order = 'zzz[artillery-bombardment-remote]',

	icon = '__dbots-artillery-bombardment-remote__/graphics/icons/artillery-bombardment-remote.png',
	icon_size = 32,

	flags = {"only-in-cursor", "not-stackable", "spawnable"},
	stack_size = 1,
	stackable = false,

	select = {
		border_color = {r = 1, g = 0.28, b = 0, a = 1},
		mode = {'enemy'},
		cursor_box_type = 'entity',
	},

	alt_select = {
		border_color = {r = 0, g = 0, b = 1, a = 1},
		mode = {'enemy'},
		cursor_box_type = 'entity',
	},
}

local smart_item = {
	type = 'selection-tool',
	name = 'smart-artillery-bombardment-remote',
	subgroup = 'capsule',
	order = 'zzz[smart-artillery-bombardment-remote]',

	icon = '__dbots-artillery-bombardment-remote__/graphics/icons/smart-artillery-bombardment-remote.png',
	icon_size = 32,

	flags = {"only-in-cursor", "not-stackable", "spawnable"},
	stack_size = 1,
	stackable = false,

	select = {
		border_color = {r = 1, g = 0.28, b = 0, a = 1},
		mode = {'enemy'},
		cursor_box_type = 'entity',
	},

	alt_select = {
		border_color = {r = 0, g = 0, b = 1, a = 1},
		mode = {'enemy'},
		cursor_box_type = 'entity',
	},
}

local exploration_item = {
	type = 'selection-tool',
	name = 'smart-artillery-exploration-remote',
	subgroup = 'capsule',
	order = 'zzz[smart-artillery-exploration-remote]',

	icon = '__dbots-artillery-bombardment-remote__/graphics/icons/smart-artillery-exploration-remote.png',
	icon_size = 32,

	flags = {"only-in-cursor", "not-stackable", "spawnable"},
	stack_size = 1,
	stackable = false,

	select = {
		border_color = {r = 1, g = 0.28, b = 0, a = 1},
		mode = {'same-force'},
		cursor_box_type = 'entity',
		entity_filters = {'artillery-turret', 'artillery-wagon'}
	},

	alt_select = {
		border_color = {r = 0, g = 0, b = 1, a = 1},
		mode = {'enemy'},
		cursor_box_type = 'entity',
		entity_filters = {'artillery-turret', 'artillery-wagon'}
	},
}

local original_tech = table.deepcopy(data.raw.technology['artillery'])

original_tech.unit.ingredients = {
	{'automation-science-pack', 1},
	{'logistic-science-pack', 1},
	{'chemical-science-pack', 1},
	{'military-science-pack', 1},
	{'utility-science-pack', 1},
	{'space-science-pack', 1}
}

original_tech.icons = {
	{
		icon = table.deepcopy(original_tech.icon),
		icon_size = table.deepcopy(original_tech.icon_size)
	}
}

local technology = table.deepcopy(original_tech)
technology.name = 'artillery-bombardment-remote'
technology.effects = {}

technology.localised_name = {'item-name.artillery-bombardment-remote'}
technology.localised_description = {'item-description.artillery-bombardment-remote'}

technology.prerequisites = {'artillery'}
technology.unit.count = 2000
technology.order = 'd-e-f-y'

table.insert(technology.icons, {
	icon = '__dbots-artillery-bombardment-remote__/graphics/icons/artillery-bombardment-remote.png',
	icon_size = 32,
	scale = 2,
	shift = {98, 98}
})

local smart_technology = table.deepcopy(original_tech)
smart_technology.name = 'smart-artillery-bombardment-remote'
smart_technology.effects = {}

smart_technology.localised_name = {'item-name.smart-artillery-bombardment-remote'}
smart_technology.localised_description = {'item-description.smart-artillery-bombardment-remote'}

smart_technology.prerequisites = {'artillery-bombardment-remote'}
smart_technology.unit.count = 4000
smart_technology.order = 'd-e-f-z'

table.insert(smart_technology.icons, {
	icon = '__dbots-artillery-bombardment-remote__/graphics/icons/smart-artillery-bombardment-remote.png',
	icon_size = 32,
	scale = 2,
	shift = {98, 98}
})

local exploration_technology = table.deepcopy(original_tech)
exploration_technology.name = 'smart-artillery-exploration-remote'
exploration_technology.effects = {}

exploration_technology.localised_name = {'item-name.smart-artillery-exploration-remote'}
exploration_technology.localised_description = {'item-description.smart-artillery-exploration-remote'}

exploration_technology.prerequisites = {'artillery'}
exploration_technology.unit.count = 2000
exploration_technology.order = 'd-e-f-z'

table.insert(exploration_technology.icons, {
	icon = '__dbots-artillery-bombardment-remote__/graphics/icons/smart-artillery-exploration-remote.png',
	icon_size = 32,
	scale = 2,
	shift = {98, 98}
})

local function make_shortcut(item)
	return {
		type = 'shortcut',
		name = item.name,
		localised_name = item.localised_name,

		order = 'e[spidertron-remote]',
		unavailable_until_unlocked = true,

		associated_control_input = item.name,
		action = 'spawn-item',
		item_to_spawn = item.name,
		technology_to_unlock = item.name,
		icons = {{
			icon = item.icon,
			icon_size = item.icon_size,
			scale = 1,
		}},
		small_icons = {{
			icon = item.icon,
			icon_size = item.icon_size,
			scale = 1,
		}},
	}
end

data:extend({
	item, make_shortcut(item),
	smart_item, make_shortcut(smart_item),
	exploration_item, make_shortcut(exploration_item),
	technology, smart_technology, exploration_technology
})
