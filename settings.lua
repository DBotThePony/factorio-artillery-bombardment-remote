
data:extend({
	{
		type = 'int-setting',
		name = 'artillery-bombardment-remote-hard-limit',
		setting_type = 'runtime-global',
		minimum_value = 30,
		default_value = 1000,
		order = 'artillery-bombardment-remote-a1',
	},

	{
		type = 'bool-setting',
		name = 'artillery-bombardment-remote-target-any-hostile-force',
		setting_type = 'runtime-global',
		default_value = true,
		order = 'artillery-bombardment-remote-a2',
	},

	{
		type = 'bool-setting',
		name = 'artillery-bombardment-remote-target-any-hostile-force-player',
		localised_name = {'mod-setting-name.artillery-bombardment-remote-target-any-hostile-force'},
		setting_type = 'runtime-per-user',
		default_value = true,
		order = 'artillery-bombardment-remote-b',
	},

	{
		type = 'double-setting',
		name = 'artillery-bombardment-remote-x-spacing',
		setting_type = 'runtime-per-user',
		minimum_value = 1,
		default_value = 6,
		order = 'artillery-bombardment-remote-item-a1',
	},

	{
		type = 'double-setting',
		name = 'artillery-bombardment-remote-y-spacing',
		setting_type = 'runtime-per-user',
		minimum_value = 1,
		default_value = 4,
		order = 'artillery-bombardment-remote-item-a2',
	},

	{
		type = 'int-setting',
		name = 'artillery-bombardment-remote-column-offset',
		setting_type = 'runtime-per-user',
		minimum_value = 1,
		default_value = 2,
		order = 'artillery-bombardment-remote-item-a3',
	},

	{
		type = 'double-setting',
		name = 'artillery-bombardment-remote-overkill-radius',
		setting_type = 'runtime-per-user',
		minimum_value = 0,
		default_value = 6,
		order = 'artillery-bombardment-remote-item-b1',
	},
})
