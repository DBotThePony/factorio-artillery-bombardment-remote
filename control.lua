
local function position_to_chunk(position)
	return {
		x = math.floor(position.x / 32),
		y = math.floor(position.y / 32)
	}
end

-- Aidiakapi#2177
-- https://gist.github.com/Aidiakapi/a1b4e603c37f1121686d372e542d4339
local function chunk_to_chunkid(chunk_x, chunk_y)
	if chunk_x < 0 then
		chunk_x = 0x10000 + chunk_x
	end

	if chunk_y < 0 then
		chunk_y = 0x10000 + chunk_y
	end

	return bit32.bor(chunk_x, bit32.lshift(chunk_y, 16))
end

-- Aidiakapi#2177
-- https://gist.github.com/Aidiakapi/a1b4e603c37f1121686d372e542d4339
local function chunkid_to_chunk(chunkid)
	local chunk_x = bit32.band(chunkid, 0xffff)
	local chunk_y = bit32.rshift(chunkid, 16)

	if chunk_x >= 0x8000 then
		chunk_x = chunk_x - 0x10000
	end

	if chunk_y >= 0x8000 then
		chunk_y = chunk_y - 0x10000
	end

	return chunk_x, chunk_y
end

-- EnigmaticAussie#9641
local function chunkid_to_chunk_position(chunkid)
	local chunk_x, chunk_y = chunkid_to_chunk(chunkid)
	chunk_x = chunk_x * 32 + 16
	chunk_y = chunk_y * 32 + 16
	return chunk_x, chunk_y
end

local function on_player_selected_area(event)
	if event.item == 'artillery-bombardment-remote' then
		local ply = game.players[event.player_index]
		local surface = ply.surface
		local force = ply.force

		local x_spacing = ply.mod_settings['artillery-bombardment-remote-x-spacing'].value
		local y_spacing = ply.mod_settings['artillery-bombardment-remote-y-spacing'].value
		local column_offset = ply.mod_settings['artillery-bombardment-remote-column-offset'].value

		local hard_limit = settings.global['artillery-bombardment-remote-hard-limit'].value

		local count = 0
		local col_count = 0

		for x = event.area.left_top.x, event.area.right_bottom.x, x_spacing do
			local start_y = event.area.left_top.y + column_offset * (col_count % column_offset)

			for y = start_y, event.area.right_bottom.y, y_spacing do
				surface.create_entity({
					name = 'artillery-flare',
					position = {x, y},
					force = force,
					movement = {0, 0},
					height = 0,
					vertical_speed = 0,
					frame_speed = 0
				})

				count = count + 1

				if count >= hard_limit then
					break
				end
			end

			col_count = col_count + 1

			if count >= hard_limit then
				ply.print({'artillery-bombardment-remote.shot_limit_reached', hard_limit})
				break
			end
		end
	elseif event.item == 'smart-artillery-bombardment-remote' then
		local ply = game.players[event.player_index]
		local surface = ply.surface
		local force = ply.force

		local overkill_radius = ply.mod_settings['artillery-bombardment-remote-overkill-radius'].value
		overkill_radius = overkill_radius * overkill_radius

		local points_hit = {}

		if event.area.left_top.x == event.area.right_bottom.x or event.area.left_top.y == event.area.right_bottom.y then
			return
		end

		local find_forces = 'enemy'

		if
			settings.global['artillery-bombardment-remote-target-any-hostile-force'] and -- game allows bombing any hostile force
			ply.mod_settings['artillery-bombardment-remote-target-any-hostile-force-player'].value -- player wants to bomb any hostile force
		then
			find_forces = {}

			for name, game_force in pairs(game.forces) do
				if game_force.name ~= force.name and not force.is_friend(game_force) then
					table.insert(find_forces, game_force)
				end
			end
		end

		-- Find enemy military structures in selection area, mark with flare
		local military_structures = surface.find_entities_filtered({
			area = event.area,
			force = find_forces,
			type = {'unit-spawner', 'turret', 'simple-entity-with-force', 'radar', 'player-port'}
		})

		local is_cheat_mode = ply.cheat_mode
		local is_chunk_charted = force.is_chunk_charted -- bind Lua method early to avoid excessive rebind
		local hard_limit = settings.global['artillery-bombardment-remote-hard-limit'].value
		local position_indexes = 1

		for _, entity in ipairs(military_structures) do
			if not is_cheat_mode and not is_chunk_charted(surface, position_to_chunk(entity.position)) then goto CONTINUE end

			local entity_position = entity.position

			if overkill_radius > 0 then
				for i = 1, position_indexes - 1 do
					local position = points_hit[i]
					local x_dist = entity_position.x - position.x
					local y_dist = entity_position.y - position.y

					if x_dist * x_dist + y_dist * y_dist < overkill_radius then
						goto CONTINUE
					end
				end
			end

			surface.create_entity({
				name = 'artillery-flare',
				position = entity_position,
				force = force,
				movement = {0, 0},
				height = 0,
				vertical_speed = 0,
				frame_speed = 0
			})

			if overkill_radius > 0 then
				points_hit[position_indexes] = entity_position
			end

			position_indexes = position_indexes + 1

			if position_indexes >= hard_limit then
				break
			end

			::CONTINUE::
		end

		-- Mark chunks with flare for exploration position_to_chunk

		if position_indexes < hard_limit then
			local left_top_chunk = position_to_chunk(event.area.left_top)
			local right_bottom_chunk = position_to_chunk(event.area.right_bottom)

			for x = left_top_chunk.x, right_bottom_chunk.x do
				for y = left_top_chunk.y, right_bottom_chunk.y do
					if not is_chunk_charted(surface, {x, y}) then
						surface.create_entity({
							name = 'artillery-flare',
							position = {(x * 32) + 16, (y * 32) + 16},
							force = force,
							movement = {0, 0},
							height = 0,
							vertical_speed = 0,
							frame_speed = 0
						})

						position_indexes = position_indexes + 1

						if position_indexes >= hard_limit then
							goto HARD_LIMIT_HIT
						end
					end
				end
			end

			::HARD_LIMIT_HIT::
		end

		if position_indexes >= hard_limit then
			ply.print({'artillery-bombardment-remote.shot_limit_reached', hard_limit})
		end
	elseif event.item == 'smart-artillery-exploration-remote' then
		local id = event.player_index
		local player = game.players[id]
		local surface = player.surface
		local force = player.force

		-- Find artillery in selection area, add it's chunk to the global list
		-- Have on_tick handler manage the flare markers
		local artillery = surface.find_entities_filtered({
			area = event.area,
			force = 'player',
			type = {'artillery-turret', 'artillery-wagon'}
		})

		-- get artillery range, multiply by bonus, minus 2/3rds of a chunk for accuracy
		local artillery_range =
			(prototypes.item['artillery-wagon-cannon'].attack_parameters.range) *
			(1 + player.force.artillery_range_modifier) * 2.5 - (2 * 32 / 3)

		-- for each artillery turret, add it's chunk to the global list
		for _, entity in ipairs(artillery) do
			local artillery_chunk_position = position_to_chunk(entity.position)
			local artillery_chunk_id = chunk_to_chunkid(artillery_chunk_position.x, artillery_chunk_position.y)

			if not storage.chunks[artillery_chunk_id] then
				storage.chunks[artillery_chunk_id] = {
					force = force,
					surface = surface,
					i = 1,
					artillery_range = artillery_range
				}
			end
		end
	end
end

script.on_event(defines.events.on_player_selected_area, on_player_selected_area)

local function on_player_alt_selected_area(event)
	if event.item == 'artillery-bombardment-remote' or event.item == 'smart-artillery-bombardment-remote' then
		if event.area.left_top.x == event.area.right_bottom.x or event.area.left_top.y == event.area.right_bottom.y then
			return
		end

		local flares = game.players[event.player_index].surface.find_entities_filtered({
			area = event.area,
			name = 'artillery-flare',
			force = game.players[event.player_index].force
		})

		for _, flare in ipairs(flares) do
			flare.destroy()
		end
	end
end

script.on_event(defines.events.on_player_alt_selected_area, on_player_alt_selected_area)

local function on_init(event)
	storage.chunks = {}
end

script.on_init(on_init)

local function on_configuration_changed(event)
	storage.chunks = storage.chunks or {}
end

script.on_configuration_changed(on_configuration_changed)

local function on_nth_tick()
	-- on_tick handler for iterating over artillery turrets to mark max distance flares
	-- currently limited to 3 points every 5 ticks to reduce UPS loss
	-- currently processes the first chunk id in the available chunks list
	local chunkid, data = next(storage.chunks)

	if chunkid ~= nil then
		--log(serpent.block(chunkid))
		--log(serpent.block(data))

		local chunk_pos_x, chunk_pos_y = chunkid_to_chunk_position(chunkid)
		local surface = data.surface
		local force = data.force
		-- limited to 3 calculations per 5 ticks
		for i = data.i, data.i + 3 do
			-- get angle
			local angle = i * math.pi / 180
			-- get co-ordinates
			local ptx = chunk_pos_x + data.artillery_range * math.cos(angle)
			local pty = chunk_pos_y + data.artillery_range * math.sin(angle)
			-- get chunk at that position
			local attack_chunk_pos = position_to_chunk({x = ptx, y = pty})
			-- check if charted
			if not force.is_chunk_charted(surface, {attack_chunk_pos.x, attack_chunk_pos.y}) then
				--log(serpent.block(attack_chunk_pos))
				-- Mark chunks with flare for exploration
				surface.create_entity(
					{
						name = 'artillery-flare',
						position = {(attack_chunk_pos.x * 32) + 16, (attack_chunk_pos.y * 32) + 16},
						force = force,
						movement = {0, 0},
						height = 0,
						vertical_speed = 0,
						frame_speed = 0
					}
				)
			end
		end
		-- remove finished artillery positions
		if data.i + 1 >= 360 then
			data = nil
			storage.chunks[chunkid] = nil
		else
			data.i = data.i + 3
		end
	end
end

script.on_nth_tick(5, on_nth_tick)
